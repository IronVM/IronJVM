/* SPDX-License-Identifier: GPL-2.0
 *
 * IronJVM: JVM Implementation in Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use ironjvm_jcfspec::attribute_info::AttributeInfo;
use std::ptr::slice_from_raw_parts;
use std::slice;

use ironjvm_jcfspec::cp_info::{ConstantPoolInfo, ConstantPoolInfoInner};
use ironjvm_jcfspec::field_info::FieldInfo;
use ironjvm_jcfspec::types::*;
use ironjvm_jcfspec::ClassFile;

use crate::error::{ParserError, ParserResult};

mod error;

#[derive(Debug)]
pub struct Parser<'clazz> {
    bytes: &'clazz [u1],
    i: usize,
}

impl<'clazz> Parser<'clazz> {
    pub fn new(bytes: &'clazz [u1]) -> Self {
        Self { bytes, i: 0 }
    }

    pub fn parse(&mut self) -> ParserResult<ClassFile<'clazz>> {
        let magic = self.next_u4()?;
        if magic != 0xCAFEBABE {
            return Err(ParserError::InvalidMagic);
        }

        let minor_version = self.next_u2()?;
        let major_version = self.next_u2()?;
        let constant_pool_count = self.next_u2()?;
        let constant_pool = self.parse_constant_pool(constant_pool_count)?;
        let access_flags = self.next_u2()?;
        let this_class = self.next_u2()?;
        let super_class = self.next_u2()?;
        let interfaces_count = self.next_u2()?;
        let interfaces = self.next_u2_range(interfaces_count as usize)?;
        let fields_count = self.next_u2()?;
        let fields = self.parse_fields(fields_count, &constant_pool)?;

        todo!()
    }

    fn next_u1(&mut self) -> ParserResult<u1> {
        let ret = self
            .bytes
            .first()
            .map(|b| b.clone())
            .ok_or(ParserError::UnexpectedEof)?;
        self.bytes = self
            .bytes
            .get(1..)
            .map(|b| b.clone())
            .ok_or(ParserError::UnexpectedEof)?;

        self.i += 1;
        Ok(ret)
    }

    fn next_u1_range(&mut self, len: usize) -> ParserResult<&'clazz [u1]> {
        let output = self
            .bytes
            .get(0..len as usize)
            .map(|b| b.clone())
            .ok_or(ParserError::UnexpectedEof)?;
        self.bytes = self
            .bytes
            .get(len..)
            .map(|b| b.clone())
            .ok_or(ParserError::UnexpectedEof)?;

        self.i += len;
        Ok(output)
    }

    fn next_u2(&mut self) -> ParserResult<u2> {
        Ok(u2::from_be_bytes(
            self.next_u1_range(2)?.try_into().unwrap(),
        ))
    }

    fn next_u2_range(&mut self, len: usize) -> ParserResult<&'clazz [u2]> {
        Ok(unsafe { slice::from_raw_parts(self.next_u1_range(len * 2)?.as_ptr().cast(), len) })
    }

    fn next_u4(&mut self) -> ParserResult<u4> {
        Ok(u4::from_be_bytes(
            self.next_u1_range(4)?.try_into().unwrap(),
        ))
    }

    fn parse_attributes(
        &mut self,
        len: u2,
        constant_pool: &Vec<ConstantPoolInfo<'clazz>>,
    ) -> ParserResult<Vec<AttributeInfo<'clazz>>> {
        let mut ret = Vec::with_capacity(len as usize);

        for _ in 0..len {
            let attribute_name_index = self.next_u2()?;
            let attribute_length = self.next_u4()?;

            if attribute_name_index == 0 {
                return Err(ParserError::InvalidConstantPoolIndex(attribute_name_index));
            }

            let info = constant_pool
                .get(attribute_name_index as usize - 1)
                .map(|info| info.clone())
                .ok_or(ParserError::InvalidConstantPoolIndex(attribute_name_index))?;
            if let ConstantPoolInfoInner::Utf8Info { bytes, length} = info.info {
                todo!()
            }
        }

        Ok(ret)
    }

    fn parse_constant_pool(&mut self, len: u2) -> ParserResult<Vec<ConstantPoolInfo<'clazz>>> {
        if len == 0 {
            return Err(ParserError::InvalidConstantPoolLength);
        }

        let mut ret = Vec::with_capacity(len as usize - 1);
        while ret.len() + 1 < len as usize {
            let tag = self.next_u1()?;

            let info = match tag {
                1 => {
                    let length = self.next_u2()?;

                    ConstantPoolInfoInner::Utf8Info {
                        length,
                        bytes: self.next_u1_range(length as usize).unwrap(),
                    }
                }

                3 => ConstantPoolInfoInner::IntegerInfo {
                    bytes: self.next_u4()?,
                },

                4 => ConstantPoolInfoInner::FloatInfo {
                    bytes: self.next_u4()?,
                },

                5 => ConstantPoolInfoInner::LongInfo {
                    high_bytes: self.next_u4()?,
                    low_bytes: self.next_u4()?,
                },

                6 => ConstantPoolInfoInner::DoubleInfo {
                    high_bytes: self.next_u4()?,
                    low_bytes: self.next_u4()?,
                },

                7 => ConstantPoolInfoInner::ClassInfo {
                    name_index: self.next_u2()?,
                },

                8 => ConstantPoolInfoInner::StringInfo {
                    string_index: self.next_u2()?,
                },

                9 => ConstantPoolInfoInner::FieldReferenceInfo {
                    class_index: self.next_u2()?,
                    name_and_type_index: self.next_u2()?,
                },

                10 => ConstantPoolInfoInner::MethodReferenceInfo {
                    class_index: self.next_u2()?,
                    name_and_type_index: self.next_u2()?,
                },

                11 => ConstantPoolInfoInner::InterfaceMethodReferenceInfo {
                    class_index: self.next_u2()?,
                    name_and_type_index: self.next_u2()?,
                },

                12 => ConstantPoolInfoInner::NameAndTypeInfo {
                    name_index: self.next_u2()?,
                    descriptor_index: self.next_u2()?,
                },

                15 => ConstantPoolInfoInner::MethodHandleInfo {
                    reference_kind: self.next_u1()?,
                    reference_index: self.next_u2()?,
                },

                16 => ConstantPoolInfoInner::MethodTypeInfo {
                    descriptor_index: self.next_u2()?,
                },

                17 => ConstantPoolInfoInner::DynamicInfo {
                    bootstrap_method_attr_index: self.next_u2()?,
                    name_and_type_index: self.next_u2()?,
                },

                18 => ConstantPoolInfoInner::InvokeDynamicInfo {
                    bootstrap_method_attr_index: self.next_u2()?,
                    name_and_type_index: self.next_u2()?,
                },

                19 => ConstantPoolInfoInner::ModuleInfo {
                    name_index: self.next_u2()?,
                },

                20 => ConstantPoolInfoInner::PackageInfo {
                    name_index: self.next_u2()?,
                },

                tag => return Err(ParserError::InvalidConstantPoolTag(tag)),
            };

            ret.push(ConstantPoolInfo { tag, info });
        }

        Ok(ret)
    }

    fn parse_fields(
        &mut self,
        len: u2,
        constant_pool: &Vec<ConstantPoolInfo<'clazz>>,
    ) -> ParserResult<Vec<FieldInfo<'clazz>>> {
        let mut ret = Vec::with_capacity(len as usize);

        for _ in 0..len {
            let access_flags = self.next_u2()?;
            let name_index = self.next_u2()?;
            let descriptor_index = self.next_u2()?;
            let attributes_count = self.next_u2()?;

            ret.push(FieldInfo {
                access_flags,
                name_index,
                descriptor_index,
                attributes_count,
                attributes: self.parse_attributes(attributes_count, constant_pool)?,
            });
        }

        Ok(ret)
    }
}
