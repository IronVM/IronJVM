/* SPDX-License-Identifier: GPL-2.0
 *
 * IronJVM: JVM Implementation in Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#[derive(Clone, Debug)]
pub struct ConstantPoolInfo<'clazz> {
    pub tag: u1,
    pub info: ConstantPoolInfoInner<'clazz>,
}

#[derive(Clone, Debug)]
pub enum ConstantPoolInfoInner<'clazz> {
    Utf8Info {
        length: u2,
        bytes: &'clazz [u1],
    },
    IntegerInfo {
        bytes: u4,
    },
    FloatInfo {
        bytes: u4,
    },
    LongInfo {
        high_bytes: u4,
        low_bytes: u4,
    },
    DoubleInfo {
        high_bytes: u4,
        low_bytes: u4,
    },
    ClassInfo {
        name_index: u2,
    },
    StringInfo {
        string_index: u2,
    },
    FieldReferenceInfo {
        class_index: u2,
        name_and_type_index: u2,
    },
    MethodReferenceInfo {
        class_index: u2,
        name_and_type_index: u2,
    },
    InterfaceMethodReferenceInfo {
        class_index: u2,
        name_and_type_index: u2,
    },
    NameAndTypeInfo {
        name_index: u2,
        descriptor_index: u2,
    },
    MethodHandleInfo {
        reference_kind: u1,
        reference_index: u2,
    },
    MethodTypeInfo {
        descriptor_index: u2,
    },
    DynamicInfo {
        bootstrap_method_attr_index: u2,
        name_and_type_index: u2,
    },
    InvokeDynamicInfo {
        bootstrap_method_attr_index: u2,
        name_and_type_index: u2,
    },
    ModuleInfo {
        name_index: u2,
    },
    PackageInfo {
        name_index: u2,
    },
}
