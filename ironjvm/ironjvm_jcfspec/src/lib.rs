/* SPDX-License-Identifier: GPL-2.0
 *
 * IronJVM: JVM Implementation in Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! # The IronJVM Java Class File Specification Definitions
//!
//! The `ironjvm_jcfspec` crate contains structures wrapping the structures defined in Chapter 4 of
//! the Java Virtual Machine Specification. These structures use the definitions from the
//! Specification directly.

#![allow(non_camel_case_types)]
#![feature(prelude_import)]

pub mod attribute_info;
pub mod cp_info;
pub mod field_info;
pub mod method_info;
pub mod types;

mod prelude;

use crate::attribute_info::AttributeInfo;
use crate::field_info::FieldInfo;
use crate::method_info::MethodInfo;
#[prelude_import]
pub use prelude::rust_2021::*;

#[derive(Debug)]
pub struct ClassFile<'clazz> {
    pub magic: u4,
    pub minor_version: u2,
    pub major_version: u2,
    pub constant_pool_count: u2,
    pub constant_pool: Vec<cp_info::ConstantPoolInfo<'clazz>>,
    pub access_flags: u2,
    pub this_class: u2,
    pub super_class: u2,
    pub interfaces_count: u2,
    pub interfaces: &'clazz [u2],
    pub fields_count: u2,
    pub fields: Vec<FieldInfo<'clazz>>,
    pub methods_count: u2,
    pub methods: Vec<MethodInfo<'clazz>>,
    pub attributes_count: u2,
    pub attributes: Vec<AttributeInfo<'clazz>>,
}
