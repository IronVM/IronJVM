/* SPDX-License-Identifier: GPL-2.0
 *
 * IronJVM: JVM Implementation in Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use crate::attribute_info::annotation::Annotation;
use crate::attribute_info::bootstrap_method::BootstrapMethod;
use crate::attribute_info::element_value_pair::ElementValue;
use crate::attribute_info::exception_table_entry::ExceptionTableEntry;
use crate::attribute_info::export::Export;
use crate::attribute_info::inner_class::InnerClass;
use crate::attribute_info::line_number_table_entry::LineNumberTableEntry;
use crate::attribute_info::local_variable_table_entry::LocalVariableTableEntry;
use crate::attribute_info::local_variable_type_table_entry::LocalVariableTypeTableEntry;
use crate::attribute_info::method_parameter::MethodParameter;
use crate::attribute_info::open::Open;
use crate::attribute_info::parameter_annotation::ParameterAnnotation;
use crate::attribute_info::provide::Provide;
use crate::attribute_info::record_component_info::RecordComponentInfo;
use crate::attribute_info::require::Require;
use crate::attribute_info::stack_map_frame::StackMapFrame;
use crate::attribute_info::type_annotation::TypeAnnotation;

pub mod annotation;
pub mod bootstrap_method;
pub mod element_value_pair;
pub mod exception_table_entry;
pub mod export;
pub mod inner_class;
pub mod line_number_table_entry;
pub mod local_variable_table_entry;
pub mod local_variable_target_table_entry;
pub mod local_variable_type_table_entry;
pub mod method_parameter;
pub mod open;
pub mod parameter_annotation;
pub mod provide;
pub mod record_component_info;
pub mod require;
pub mod stack_map_frame;
pub mod type_annotation;
pub mod verification_type_info;

#[derive(Debug)]
pub struct AttributeInfo<'clazz> {
    pub attribute_name_index: u2,
    pub attribute_length: u4,
    pub info: AttributeInfoInner<'clazz>,
}

#[derive(Debug)]
pub enum AttributeInfoInner<'clazz> {
    ConstantValueAttribute {
        constvalue_index: u2,
    },
    CodeAttribute {
        max_stack: u2,
        max_locals: u2,
        code_length: u4,
        code: &'clazz [u1],
        exception_table_length: u2,
        exception_table: Vec<ExceptionTableEntry>,
        attributes_count: u2,
        attributes: Vec<AttributeInfo<'clazz>>,
    },
    StackMapTableAttribute {
        number_of_entries: u2,
        entries: Vec<StackMapFrame>,
    },
    ExceptionsAttribute {
        number_of_exceptions: u2,
        exception_index_table: &'clazz [u2],
    },
    InnerClassesAttribute {
        number_of_classes: u2,
        classes: Vec<InnerClass>,
    },
    EnclosingMethodAttribute {
        class_index: u2,
        method_index: u2,
    },
    SyntheticAttribute,
    SignatureAttribute {
        signature_index: u2,
    },
    SourceFileAttribute {
        sourcefile_index: u2,
    },
    SourceDebugExtensionAttribute {
        debug_extension: &'clazz [u1],
    },
    LineNumberTableAttribute {
        line_number_table_length: u2,
        line_number_table: Vec<LineNumberTableEntry>,
    },
    LocalVariableTableAttribute {
        local_variable_table_length: u2,
        local_variable_table: Vec<LocalVariableTableEntry>,
    },
    LocalVariableTypeTableAttribute {
        local_variable_type_table_length: u2,
        local_variable_type_table: Vec<LocalVariableTypeTableEntry>,
    },
    DeprecatedAttribute,
    RuntimeVisibleAnnotationsAttribute {
        num_annotations: u2,
        annotations: Vec<Annotation>,
    },
    RuntimeInvisibleAnnotationsAttribute {
        num_annotations: u2,
        annotations: Vec<Annotation>,
    },
    RuntimeVisibleParameterAnnotationsAttribute {
        num_parameters: u2,
        parameter_annotations: Vec<ParameterAnnotation>,
    },
    RuntimeInvisibleParameterAnnotationsAttribute {
        num_parameters: u2,
        parameter_annotations: Vec<ParameterAnnotation>,
    },
    RuntimeVisibleTypeAnnotationsAttribute {
        num_annotations: u2,
        annotations: Vec<TypeAnnotation>,
    },
    RuntimeInvisibleTypeAnnotationsAttribute {
        num_annotations: u2,
        annotations: Vec<TypeAnnotation>,
    },
    AnnotationDefaultAttribute {
        default_value: ElementValue,
    },
    BootstrapMethodsAttribute {
        num_bootstrap_methods: u2,
        bootstrap_methods: Vec<BootstrapMethod<'clazz>>,
    },
    MethodParameters {
        parameters_count: u1,
        parameters: Vec<MethodParameter>,
    },
    ModuleAttribute {
        module_name_index: u2,
        module_flags: u2,
        module_version_index: u2,

        requires_count: u2,
        requires: Vec<Require>,

        exports_count: u2,
        exports: Vec<Export<'clazz>>,

        opens_count: u2,
        opens: Vec<Open<'clazz>>,

        uses_count: u2,
        uses_index: &'clazz [u2],

        provides_count: u2,
        provides: Vec<Provide<'clazz>>,
    },
    ModulePackagesAttribute {
        package_count: u2,
        package_index: &'clazz [u2],
    },
    ModuleMainClassAttribute {
        main_class_index: u2,
    },
    NestHostAttribute {
        host_index: u2,
    },
    NestMembersAttribute {
        number_of_classes: u2,
        classes: &'clazz [u2],
    },
    RecordAttribute {
        components_count: u2,
        components: Vec<RecordComponentInfo<'clazz>>,
    },
    PermittedSubclassesAttribute {
        number_of_classes: u2,
        classes: &'clazz [u2],
    },
}
