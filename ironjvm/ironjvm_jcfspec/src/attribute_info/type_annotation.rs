/* SPDX-License-Identifier: GPL-2.0
 *
 * IronJVM: JVM Implementation in Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use crate::attribute_info::element_value_pair::ElementValuePair;
use crate::attribute_info::local_variable_target_table_entry::LocalVariableTargetTableEntry;

#[derive(Debug)]
pub struct TypeAnnotation {
    pub target_type: u1,
    pub target_info: TypeAnnotationTargetInfo,
    pub target_path: TypePath,
    pub type_index: u2,
    pub num_element_value_pairs: u2,
    pub element_value_pairs: Vec<ElementValuePair>,
}

#[derive(Debug)]
pub struct TypePath {
    pub path_length: u1,
    pub path: Vec<TypePathSegment>,
}

#[derive(Debug)]
pub struct TypePathSegment {
    pub type_path_kind: u1,
    pub type_argument_index: u1,
}

#[derive(Debug)]
pub enum TypeAnnotationTargetInfo {
    TypeParameterTarget {
        type_parameter_index: u1,
    },
    SuperTypeTarget {
        super_type_index: u2,
    },
    TypeParameterBoundTarget {
        type_parameter_index: u1,
        bound_index: u1,
    },
    EmptyTarget,
    FormalParameterTarget {
        formal_parameter_index: u1,
    },
    ThrowsTarget {
        throws_type_index: u2,
    },
    LocalVariableTarget {
        table_length: u2,
        table: Vec<LocalVariableTargetTableEntry>,
    },
    CatchTarget {
        exception_table_index: u2,
    },
    OffsetTarget {
        offset: u2,
    },
    TypeArgumentTarget {
        type_argument_index: u1,
    },
}
