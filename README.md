![IronJVM](./meta/images/ironjvmbanner.png)

<h1 align="center">
    IronJVM
</h1>

> ⚠️**Being incredibly a work in progress and under rapid development, the IronJVM should not be used in production under most circumstances.**

This README is for *users* rather than *contributors*,

## Quick Start

## Building from Source

## License

IronJVM is distributed under the **GNU General Public License Version 2.0**. See [COPYING](https://codeberg.org/IronVM/IronJVM/src/branch/main/COPYING) for more details.
